from base.foursevenfour import FourSevenFour

import numpy as np
import os
os.chdir(r'/Users/Untitled/Documents/ARUM/Akademik/Semester_8/IF4020 Kriptografi/UTS1/474algorithm/mode')

class OutputFeedBack(object):
    def __init__(self):
        self.len_block = 16
    
    def _get_len_block(self):
        return self.len_block

    def read_file(self, filename):
        data = None
        with open(filename, 'r') as file:
            data = file.read()
        return data

    def write_file(self, filename, data):
        with open(filename, 'w') as file:
            file.write(data)
    
    def _get_seed_number(self, key):
        seed_number = 0
        for char in key :
            seed_number = seed_number + ord(char)
        return seed_number
    
    def __call__(self, filename, key, proc):
        f = FourSevenFour()
        text = self.read_file(filename)
        np.random.seed(self._get_seed_number(key))
        initial_vector = np.random.choice(255, size=(16)).tolist()

        list_block = self.preprocess_block(text)

        if proc == 'encrypt':
            print(proc)
            
            inc = 0
            list_cipher = []
            queue = initial_vector
            while inc < len(list_block):
            # TO-DO define encryption function here
                list_encrypt = f(queue, key, 'encrypt')
            #======================================
            
                list_cipher.append(list_encrypt[0] ^ list_block[inc])
                queue.append(list_encrypt[0])
                queue.pop(0)
                inc+=1
            
            list_result = list(map(chr, list_cipher))
            
        elif proc == 'decrypt':
            print(proc)

            inc = 0
            list_plain = []
            temp = []
            queue = initial_vector
            while inc < len(list_block):
            # TO-DO define decryption function here
                list_decrypt = f(queue, key, 'encrypt')
            #======================================

                list_plain.append(list_decrypt[0] ^ list_block[inc])
                queue.append(list_decrypt[0])
                queue.pop(0)
                inc+=1
            
            list_result = list(map(chr, list_plain))
        
        self.write_file('output.txt', ''.join(list_result))

    def preprocess_block(self,text):
        list_char = list(text)
        list_int = list(map(ord, list_char))
        return list_int

if __name__ == '__main__':
    ofb = OutputFeedBack()
    # cipherresult = ofb('input.txt', 'helo','encrypt')
    plainresult = ofb('output.txt', 'helo','decrypt')