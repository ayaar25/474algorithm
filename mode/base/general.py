def bit_to_int(bit, bit_size=8):
  integers = []
  for i in range(0, len(bit), bit_size):
    integer = 0
    for x in bit[i:i+bit_size]:
      integer = (integer << 1) + x
    integers.append(integer)
  return integers

def chunk_list(_list, chunk_size):
  return [_list[i:i+chunk_size] for i in range(0, len(_list), chunk_size)]

def empty_list(colsize=1):
  _list = []
  for _ in range(0, colsize):
    _list.append(None)
  return _list

def hex_size(integer):
  i = 1
  while integer > 15:
    integer = integer >> 4
    i += 1
  return i  

def int_to_bit(integer, bit_size=8):
  bits = []
  for x in integer:
    bits = bits + list(map(int, list(bin(x).lstrip('0b').rjust(8, '0'))))
  return bits

def print_hex_list(_list):
  for col in _list:
    print(hex(col), ' ', end='')
  print()

def print_hex_matrice(matrice):
  for row in matrice:
    for col in row:
      print(hex(col), ' ', end='')
    print()

def split_hex(integer, size=2):
  try:
    if size < 0:
      raise Error(ERR_BAD_VALUE, 'Size should be larger or equal to 0')
    return divmod(integer, 0x1 << 2**size)
  except Error as e:
    e.print()

def unsign_bit(integer):
  mask_bit = 0xf
  for x in range(1, hex_size(integer)):
    mask_bit = (mask_bit << 4) + 0xf
  return ~integer & mask_bit

ERR_BAD_VALUE = {
  'code': -1,
  'title': '[INVALID VALUE]'
}

ERR_INTERNAL = {
  'code': -1,
  'title': '[INTERNAL ERROR]'
}

ERR_UNKNOWN = {
  'code': -1,
  'title': '[UNKNOWN ERROR]'
}

class Error(Exception):
  def __init__(self, error=ERR_UNKNOWN, description=None):
    self.error = error
    self.error['description'] = description

  @property
  def code(self):
    return self.error['code']

  @property
  def title(self):
    return self.error['title']

  @property
  def description(self):
    return self.error['description']

  def print(self):
    print(self.title)
    if self.description != None:
      print(self.description)